<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $embek = new Sheep("Shaun The Sheep");
    echo "Name : " .$embek->name . "<br>";
    echo "Legs : " .$embek->legs . "<br>";
    echo "Cold Blooded : " .$embek->cold_blooded . "<br> <br>";

    $frog = new Kodok("Pangeran Kodok");
    echo "Name : " .$frog->name . "<br>";
    echo "Legs : " .$frog->legs . "<br>";
    echo "Cold Blooded : " .$frog->cold_blooded . "<br>";
    echo "Jump : " . $frog->jump() . "<br> <br>";

    $ape = new Kera("SunGoKong");
    echo "Name : " .$ape->name . "<br>";
    echo "Legs : " .$ape->legs . "<br>";
    echo "Cold Blooded : " .$ape->cold_blooded . "<br>";
    echo "Yell : " . $ape->yell();
?>